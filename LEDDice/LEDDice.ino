int diceRoll = 0;

void setup() {
  for(int x=1;x<7;++x) {
    pinMode(x, OUTPUT);
  }

  randomSeed(analogRead(0));
  diceRoll = random(2,7);
}

void loop() {
  for(int x=1;x<7;++x) {
    digitalWrite(x,LOW);
  }

  delay(1000);
  
  for(int x=1;x<diceRoll;++x) {
    digitalWrite(x,HIGH);
  }

  delay(1000);
}
